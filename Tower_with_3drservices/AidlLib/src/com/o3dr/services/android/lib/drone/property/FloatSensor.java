package com.o3dr.services.android.lib.drone.property;

import android.os.Parcel;
import android.os.Parcelable;


public class FloatSensor implements Parcelable {
    private double latitude;
    private double longitude;
    private double altitude;
    private long time;
    private float sensorValue;


    public FloatSensor(){}

    public FloatSensor(double latitude, double longitude, double altitude, long time, float sensorValue){
        this.setLatitude(latitude);
        this.setLongitude(longitude);
        this.setAltitude(altitude);
        this.setTime(time);
        this.setSensorValue(sensorValue);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FloatSensor)) return false;

        FloatSensor sensor = (FloatSensor) o;

        if(Double.compare(sensor.altitude, getAltitude()) != 0) return false;
        if(Double.compare(sensor.latitude, getLatitude()) != 0) return false;
        if(Double.compare(sensor.longitude, getLongitude()) != 0) return false;
        if(sensor.time != getTime()) return false;
     //   if(sensor.sensorType != null || !sensor.sensorType.equals(getSensorType())) return false;
        if(sensor.sensorValue != getSensorValue()) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getAltitude());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLatitude());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLongitude());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Float.floatToIntBits(getSensorValue());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = time;
        result = 31 * result + (int) (temp ^ (temp >>> 32));

        return result;
    }

    @Override
    public String toString() {
        return "Sensor{" +
                "latitude=" + getLatitude() +
                ", longitude=" + getLongitude() +
                ", altitude=" + getAltitude() +
                ", time=" + getTime() +
                ", sensorValue=" + getSensorValue() +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeDouble(this.altitude);
        dest.writeLong(this.time);
        dest.writeFloat(this.sensorValue);
    }

    private FloatSensor(Parcel in){
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.altitude = in.readDouble();
        this.time = in.readLong();
        this.sensorValue = in.readFloat();
    }


    public static final Creator<FloatSensor> CREATOR = new Creator<FloatSensor>() {
        public FloatSensor createFromParcel(Parcel source) {
            return new FloatSensor(source);
        }

        public FloatSensor[] newArray(int size) {
            return new FloatSensor[size];
        }
    };

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public float getSensorValue() {
        return sensorValue;
    }

    public void setSensorValue(float sensorValue) {
        this.sensorValue = sensorValue;
    }
}
