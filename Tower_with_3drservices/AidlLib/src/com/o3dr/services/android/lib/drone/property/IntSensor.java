package com.o3dr.services.android.lib.drone.property;

import android.os.Parcel;
import android.os.Parcelable;

/**************************************************************
 *
 * This class is the one, that the client will have access to.
 *
 *************************************************************/
public class IntSensor implements Parcelable {
    private double latitude;
    private double longitude;
    private double altitude;
    private long time;
    private int sensorValue;
    private static int globalMin = 0;
    private static int globalMax = 0;


    public IntSensor(){}

    public IntSensor(double latitude, double longitude, double altitude, long time, int sensorValue){
        this.setLatitude(latitude);
        this.setLongitude(longitude);
        this.setAltitude(altitude);
        this.setTime(time);
        this.setSensorValue(sensorValue);
        updateGlobalMinMax();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IntSensor)) return false;

        IntSensor sensor = (IntSensor) o;

        if(Double.compare(sensor.altitude, getAltitude()) != 0) return false;
        if(Double.compare(sensor.latitude, getLatitude()) != 0) return false;
        if(Double.compare(sensor.longitude, getLongitude()) != 0) return false;
        if(sensor.time != getTime()) return false;
     //   if(sensor.sensorType != null || !sensor.sensorType.equals(getSensorType())) return false;
        if(sensor.sensorValue != getSensorValue()) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getAltitude());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLatitude());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLongitude());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = getSensorValue();
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = time;
        result = 31 * result + (int) (temp ^ (temp >>> 32));

        return result;
    }

    @Override
    public String toString() {
        return "Sensor{" +
                "latitude=" + getLatitude() +
                ", longitude=" + getLongitude() +
                ", altitude=" + getAltitude() +
                ", time=" + getTime() +
                ", sensorValue=" + getSensorValue() +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeDouble(this.altitude);
        dest.writeLong(this.time);
        dest.writeInt(this.sensorValue);
    }

    private IntSensor(Parcel in){
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.altitude = in.readDouble();
        this.time = in.readLong();
        this.sensorValue = in.readInt();
        updateGlobalMinMax();
    }

    private void updateGlobalMinMax(){
        if(sensorValue < globalMin)
            globalMin = sensorValue;
        else if(globalMax < sensorValue)
            globalMax = sensorValue;
    }


    public static final Parcelable.Creator<IntSensor> CREATOR = new Parcelable.Creator<IntSensor>() {
        public IntSensor createFromParcel(Parcel source) {
            return new IntSensor(source);
        }

        public IntSensor[] newArray(int size) {
            return new IntSensor[size];
        }
    };

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getSensorValue() {
        return sensorValue;
    }

    public void setSensorValue(int sensorValue) {
        updateGlobalMinMax();
        this.sensorValue = sensorValue;
    }

    public int getGlobalMin(){
        return globalMin;
    }

    public int getGlobalMax(){
        return globalMax;
    }
}
