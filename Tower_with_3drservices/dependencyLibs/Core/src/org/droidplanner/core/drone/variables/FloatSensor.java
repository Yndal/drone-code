package org.droidplanner.core.drone.variables;

import com.MAVLink.ardupilotmega.msg_float_sensor;

import org.droidplanner.core.drone.DroneInterfaces;
import org.droidplanner.core.drone.DroneVariable;
import org.droidplanner.core.model.Drone;


public class FloatSensor extends DroneVariable {
    private double latitude;
    private double longitude;
    private double altitude;
    private long time;
    private float sensorValue;

    public FloatSensor(Drone drone){super(drone);}

    public void setSensor(double latitude, double longitude, double altitude, long time, float sensorValue){
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.time = time;
        this.sensorValue = sensorValue;
    }

    public void setFloatSensor(msg_float_sensor msg){
        this.latitude = msg.latitude;
        this.longitude = msg.longitude;
        this.altitude = msg.altitude;
        this.time = msg.time_usec;
        this.sensorValue = msg.value;
        myDrone.notifyDroneEvent(DroneInterfaces.DroneEventsType.INT_SENSOR_DATA);
    }

    public double getAltitude() {
        return altitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public long getTime() {
        return time;
    }

    public float getSensorValue() {
        return sensorValue;
    }
}