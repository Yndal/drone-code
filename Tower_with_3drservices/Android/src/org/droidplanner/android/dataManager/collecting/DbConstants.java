package org.droidplanner.android.dataManager.collecting;

/**
 * Created by lynd on 14/03/15.
 */
public class DbConstants {
    public static class IntTable{
        public final static String _ID                = "_id";
        public final static String TABLE_NAME         = "int_table";
        public final static String TIME               = "time";
        public final static String ALTITUDE           = "altitude";
        public final static String LATITUDE           = "latitude";
        public final static String LONGITUDE          = "longitude";
        public final static String VALUE              = "value";
//        public final static String DESCRIPTION        = "description";
    }

    public static class FloatTable{
        public final static String _ID                = "_id";
        public final static String TABLE_NAME         = "float_table";
        public final static String TIME               = "time";
        public final static String ALTITUDE           = "altitude";
        public final static String LATITUDE           = "latitude";
        public final static String LONGITUDE          = "longitude";
        public final static String VALUE              = "value";
//        public final static String DESCRIPTION        = "description";
    }
}
