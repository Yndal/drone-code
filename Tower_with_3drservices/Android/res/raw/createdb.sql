CREATE TABLE int_table (_id integer primary key autoincrement, latitude real, longitude real, altitude real, time integer, value integer);
CREATE TABLE float_table (_id integer primary key autoincrement, latitude real, longitude real, altitude real, time integer, value real);
